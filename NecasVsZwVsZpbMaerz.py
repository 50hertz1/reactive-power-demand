import pandas as pd
import numpy as np
import os

import warnings
warnings.filterwarnings('ignore')

import matplotlib.pyplot as plt
import seaborn as sns

import ext_modules_helper
import edc_helper as edc

import requests
from requests_kerberos import HTTPKerberosAuth
from pandas.io.json import json_normalize
import functions as fn
#################
def Get_zaehlwerte(file):

    meta = pd.read_excel(file,skip= 1,nrows=18)

    n = meta.shape[1]
    zaehler = meta.iloc[9,2:n]
    lieferrichtung = meta.iloc[12,2:n]

    values = pd.read_excel(file).iloc[16:,2:]


    values.columns = (lieferrichtung + "_" + zaehler).values
    range1=pd.date_range('2021-03-01T00:00:00Z', '2021-03-28T02:00:00Z',freq='15T')
    range2=pd.date_range('2021-03-28T03:15:00Z', '2021-03-31T23:45:00Z',freq='15T')
    values.index = range1.append(range2)


    values['start'] = values.index
    
    values2 = pd.melt(values, id_vars=['start'])


    splitted_variable = values2['variable'].str.split("_", n = 1, expand = True) 
    values2['lieferrichtung'] = splitted_variable[0]
    values2['zaehler'] = splitted_variable[1]
    
    values3 = values2.pivot_table(index=['start','zaehler'], columns='lieferrichtung', values='value',aggfunc='first').reset_index()
    values3 = values3.fillna(0)


    values3['KW'] = values3['1.5.0'] - values3['2.5.0']
    
    return values3.pivot(index='start',columns='zaehler',values='KW')

#################Necas
files2 = os.listdir('.')
Formel1 = pd.DataFrame()


f =  '20210130 NV Ermittlung_MC_bearbeitet.xlsx'
Formel1 = pd.read_excel (f)
Formel1 = pd.DataFrame({'Zaehler':Formel1.iloc[4:272,1],
                       'Vorzeichen':Formel1.iloc[4:272,6]}).reset_index()
Formel1 = Formel1.sort_values(by='Zaehler',ascending = True).reset_index()
Formel1

files = os.listdir('202103')

x=[]
df2Einzeln=pd.DataFrame()

